from setuptools import setup

setup(
    name='palindromes',
    version='1.0.0',
    packages=['palindromes'],
    url='https://epalese@bitbucket.org/epalese/palindromes.git',
    license='Apache',
    author='epalese',
    author_email='palese.emanuelee@gmail.com',
    description='Longest palindromes.'
)
