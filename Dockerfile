FROM python:3.6-slim-stretch

ENV PYTHONPATH /src
ARG DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /src/palindromes
WORKDIR /src
COPY requirements.txt .

RUN pip install --upgrade pip && pip install \
    --no-cache-dir -r requirements.txt

COPY palindromes ./palindromes/
COPY tests ./tests
