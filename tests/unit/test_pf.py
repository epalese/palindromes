import pytest

import palindromes.pf


def test_palindrome():
    with pytest.raises(ValueError):
        palindromes.pf.Palindrome(None, None, None)
        palindromes.pf.Palindrome('', None, None)
        palindromes.pf.Palindrome('', 0, None)
        palindromes.pf.Palindrome(None, 0, 0)
        palindromes.pf.Palindrome(None, None, 0)

    p1 = palindromes.pf.Palindrome('', 0, 0)
    assert p1.original_string == ''
    assert p1.palindrome == ''
    assert p1.start_position == 0
    assert p1.end_position == 0
    assert len(p1) == 0
    assert p1.is_contained(p1) is True
    assert str(p1) == 'Text: , Index: 0, Length: 0'
    assert p1.__repr__() == '<, sp=0, ep=0>'

    p2 = palindromes.pf.Palindrome('xxxaabaayyyaa', 3, 7)
    assert p2.original_string == 'xxxaabaayyyaa'
    assert p2.palindrome == 'aabaa'
    assert p2.start_position == 3
    assert p2.end_position == 7
    assert len(p2) == 5
    assert p2.is_contained(p2) is True
    assert p2.is_contained(p1) is False
    assert p1.is_contained(p2) is False
    assert str(p2) == 'Text: aabaa, Index: 3, Length: 5'

    p3 = palindromes.pf.Palindrome('xxxaabaayyyaa', 6, 7)
    assert p3.palindrome == 'aa'
    assert len(p3) == 2
    assert p3.is_contained(p2) is True
    assert p2.is_contained(p3) is False

    p4 = palindromes.pf.Palindrome('xxxaabaayyyaa', 6, 12)
    assert p4.palindrome == 'aayyyaa'
    assert len(p4) == 7
    assert p4.is_contained(p2) is False
    assert p2.is_contained(p4) is False
    assert p3.is_contained(p4) is True


def test_LongestPalindromeFinder():
    with pytest.raises(ValueError):
        palindromes.pf.LongestPalindromeFinder(None)

    lpf1 = palindromes.pf.LongestPalindromeFinder('')
    lp = lpf1.get_longest_palindromes()
    assert len(lp) == 0
    lp = lpf1.get_longest_palindromes(unique=False)
    assert len(lp) == 0
    lp = lpf1.get_longest_palindromes(n=10)
    assert len(lp) == 0

    lpf1 = palindromes.pf.LongestPalindromeFinder('a')
    lp = lpf1.get_longest_palindromes(n=10)
    assert len(lp) == 0

    lpf2 = palindromes.pf.LongestPalindromeFinder('aba')
    lp = lpf2.get_longest_palindromes(n=2)
    assert len(lp) == 1
    assert lp[0].palindrome == 'aba'
    assert lp[0].start_position == 0
    assert len(lp[0]) == 3

    lpf3 = palindromes.pf.LongestPalindromeFinder('aabaa')
    lp = lpf3.get_longest_palindromes(n=2)
    assert len(lp) == 1
    assert lp[0].palindrome == 'aabaa'
    assert lp[0].start_position == 0
    assert len(lp[0]) == 5

    lpf4 = palindromes.pf.LongestPalindromeFinder('aabaarraba')
    lp = lpf4.get_longest_palindromes()
    assert len(lp) == 3
    assert lp[0].palindrome == 'aabaa'
    assert lp[0].start_position == 0
    assert len(lp[0]) == 5
    assert lp[1].palindrome == 'arra'
    assert lp[1].start_position == 4
    assert len(lp[1]) == 4
    assert lp[2].palindrome == 'aba'
    assert lp[2].start_position == 7
    assert len(lp[2]) == 3

    lpf5 = palindromes.pf.LongestPalindromeFinder(
        'aabaarraba',
        exclude_inner=False
    )
    lp = lpf5.get_longest_palindromes(unique=False)
    assert len(lp) == 7
    assert lp[0].palindrome == 'aabaa'
    assert lp[0].start_position == 0
    assert len(lp[0]) == 5
    assert lp[1].palindrome == 'arra'
    assert lp[1].start_position == 4
    assert len(lp[1]) == 4
    assert lp[2].palindrome == 'aba'
    assert lp[2].start_position == 1
    assert len(lp[2]) == 3
    assert lp[3].palindrome == 'aba'
    assert lp[3].start_position == 7
    assert len(lp[3]) == 3
    assert lp[4].palindrome == 'aa'
    assert lp[4].start_position == 0
    assert len(lp[4]) == 2
    assert lp[5].palindrome == 'aa'
    assert lp[5].start_position == 3
    assert len(lp[5]) == 2
    assert lp[6].palindrome == 'rr'
    assert lp[6].start_position == 5
    assert len(lp[5]) == 2


def test_example():
    s = 'sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop'
    lpf1 = palindromes.pf.LongestPalindromeFinder(s)
    lp = lpf1.get_longest_palindromes(n=3)
    assert len(lp) == 3
    assert lp[0].palindrome == 'hijkllkjih'
    assert lp[0].start_position == 23
    assert len(lp[0]) == 10
    assert lp[1].palindrome == 'defggfed'
    assert lp[1].start_position == 13
    assert len(lp[1]) == 8
    assert lp[2].palindrome == 'abccba'
    assert lp[2].start_position == 5
    assert len(lp[2]) == 6
