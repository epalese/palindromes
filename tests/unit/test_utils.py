import mock
import pytest

import palindromes.utils


def test__find_palindromes_empty():
    r = list(palindromes.utils._find_palindromes('', 0, 0))
    assert len(r) == 0


def test__find_palindromes_boundaries():
    r = list(palindromes.utils._find_palindromes('', 1, 1))
    assert len(r) == 0

    r = list(palindromes.utils._find_palindromes('a', 1, 1))
    assert len(r) == 0

    r = list(palindromes.utils._find_palindromes('aabaa', 0, 4))
    assert len(r) == 1
    assert r[0] == ('aabaa', 0, 4)

    # the check is performed only for characters
    # at position <= left_pos and >= right_pos
    # everything in the middle is not checked for palindromicity
    r = list(palindromes.utils._find_palindromes('acfga', 0, 4))
    assert len(r) == 1
    assert r[0] == ('acfga', 0, 4)

    r = list(palindromes.utils._find_palindromes('aabaa', -1, 5))
    assert len(r) == 0


def test__find_palindromes_len_1():
    r = list(palindromes.utils._find_palindromes('a', 0, 0))
    assert len(r) == 1
    assert r[0] == ('a', 0, 0)

    r = list(palindromes.utils._find_palindromes('abc', 1, 1))
    assert len(r) == 1
    assert r[0] == ('b', 1, 1)


def test__find_palindromes_even_palindromes():
    r = list(palindromes.utils._find_palindromes('abc', 0, 1))
    assert len(r) == 0

    r = list(palindromes.utils._find_palindromes('aac', 0, 1))
    assert len(r) == 1
    assert r[0] == ('aa', 0, 1)

    r = list(palindromes.utils._find_palindromes('abaabs', 2, 3))
    assert len(r) == 2
    assert r[0] == ('aa', 2, 3)
    assert r[1] == ('baab', 1, 4)


@mock.patch('palindromes.utils._find_palindromes')
def test__generate_palindromes_generators(find_palindromes):
    string = 'ab'
    for p in palindromes.utils._generate_palindromes(string, 0):
        pass
    # both yield from have to be executed
    find_palindromes.assert_has_calls([
        mock.call(string=string, left_pos=-1, right_pos=1),
        mock.call(string=string, left_pos=0, right_pos=1),
    ], any_order=True)


def test__generate_palindromes_boundaries():
    pal_list = [p for p in palindromes.utils._generate_palindromes('', 0)]
    assert len(pal_list) == 0
    pal_list = [p for p in palindromes.utils._generate_palindromes('', -1)]
    assert len(pal_list) == 0
    pal_list = [p for p in palindromes.utils._generate_palindromes('a', 0)]
    assert len(pal_list) == 0
    pal_list = [p for p in palindromes.utils._generate_palindromes('a', 1)]
    assert len(pal_list) == 0
    pal_list = [p for p in palindromes.utils._generate_palindromes('aaa', 2)]
    assert len(pal_list) == 0
    pal_list = [p for p in palindromes.utils._generate_palindromes('aaa', -1)]
    assert len(pal_list) == 0


def test__generate_palindromes_start_index():
    pal_list = [p for p in palindromes.utils._generate_palindromes('aaa', 0)]
    assert pal_list[0] == ('aa', 0, 1)
    pal_list = [p for p in palindromes.utils._generate_palindromes('aaa', 1)]
    assert len(pal_list) == 2
    assert pal_list[0] == ('aaa', 0, 2)
    assert pal_list[1] == ('aa', 1, 2)
    pal_list = [
        p for p in palindromes.utils._generate_palindromes('caabaad', 0)
    ]
    assert len(pal_list) == 0
    pal_list = [
        p for p in palindromes.utils._generate_palindromes('caabaad', 1)
    ]
    assert pal_list[0] == ('aa', 1, 2)
    pal_list = [
        p for p in palindromes.utils._generate_palindromes('caabaad', 2)
    ]
    assert len(pal_list) == 0
    pal_list = [
        p for p in palindromes.utils._generate_palindromes('caabaad', 3)
    ]
    assert len(pal_list) == 2
    assert pal_list[0] == ('aba', 2, 4)
    assert pal_list[1] == ('aabaa', 1, 5)


def test__generate_palindromes_no_inner_boundaries():
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner('', 0)
    ]
    assert len(pal_list) == 0
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner('', -1)
    ]
    assert len(pal_list) == 0
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner('a', 0)
    ]
    assert len(pal_list) == 0
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner('a', 1)
    ]
    assert len(pal_list) == 0
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner('aaa', 2)
    ]
    assert len(pal_list) == 0
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner('aaa', -1)
    ]
    assert len(pal_list) == 0


def test__generate_palindromes_no_inner_start_index():
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner('aaa', 0)
    ]
    assert pal_list[0] == ('aa', 0, 1)
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner('aaa', 1)
    ]
    assert len(pal_list) == 2
    assert pal_list[0] == ('aaa', 0, 2)
    assert pal_list[1] == ('aa', 1, 2)
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner(
            'caabaad', 0
        )
    ]
    assert len(pal_list) == 0
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner(
            'caabaad', 1
        )
    ]
    assert pal_list[0] == ('aa', 1, 2)
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner(
            'caabaad', 2
        )
    ]
    assert len(pal_list) == 0
    # Note: wrt to _generate_palindromes here I expect only the longest
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner(
            'caabaad', 3
        )
    ]
    # ('aba', 2, 4) is excluded
    assert len(pal_list) == 1
    assert pal_list[0] == ('aabaa', 1, 5)
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner(
            'caabaadaa', 3
        )
    ]
    assert len(pal_list) == 1
    assert pal_list[0] == ('aabaa', 1, 5)
    # test overlapping palindromes
    pal_list = [
        p for p in palindromes.utils._generate_palindromes_no_inner(
            'caabaadaa', 6
        )
    ]
    assert len(pal_list) == 1
    assert pal_list[0] == ('aadaa', 4, 8)


@mock.patch('palindromes.utils._generate_palindromes_no_inner')
@mock.patch('palindromes.utils._generate_palindromes')
def test_generate_palindromes_no_inner_call(gen_pal, gen_pal_no_inner):
    gen_pal.return_value = []
    gen_pal_no_inner.return_value = ['aba']
    gen = palindromes.utils.generate_palindromes('aba', exclude_inner=True)
    next(gen)
    assert gen_pal_no_inner.called
    assert not gen_pal.called


@mock.patch('palindromes.utils._generate_palindromes_no_inner')
@mock.patch('palindromes.utils._generate_palindromes')
def test_generate_palindromes_with_inner_call(gen_pal, gen_pal_no_inner):
    gen_pal.return_value = ['aba']
    gen_pal_no_inner.return_value = []
    gen = palindromes.utils.generate_palindromes('aba', exclude_inner=False)
    next(gen)
    assert not gen_pal_no_inner.called
    assert gen_pal.called


def test_generate_palindromes_boundaries():
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('')
    ]
    assert len(pal_list) == 0
    with pytest.raises(ValueError):
        pal_list = [
            p for p in palindromes.utils.generate_palindromes(None)
        ]
    # no palindromes of length = 1
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('a')
    ]
    assert len(pal_list) == 0


def test_generate_palindromes_no_inner():
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('aba')
    ]
    assert len(pal_list) == 1
    assert pal_list[0] == ('aba', 0, 2)

    # Note: remember that only palindromes built around the same
    # central character and shorter than the longest palindrome
    # are excluded
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('aabaa')
    ]
    assert len(pal_list) == 3
    assert pal_list[0] == ('aa', 0, 1)      # even palindrome btw 1,2
    assert pal_list[1] == ('aabaa', 0, 4)   # odd palindrome around 2
    assert pal_list[2] == ('aa', 3, 4)      # even palindrome btw 3,4
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('aabaaa')
    ]
    # I expect all as previous case, plus (aaa, 3, 5) and (aa, 4, 5)
    assert len(pal_list) == 5
    assert pal_list[0] == ('aa', 0, 1)      # even palindrome btw 1,2
    assert pal_list[1] == ('aabaa', 0, 4)   # odd palindrome around 2
    assert pal_list[2] == ('aa', 3, 4)      # even palindrome btw 3,4
    assert pal_list[3] == ('aaa', 3, 5)     # odd palindrome around 4
    assert pal_list[4] == ('aa', 4, 5)      # even palindrome btw 4,5
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('aabaaab')
    ]
    # I only expect (aaa, 3, 5) to be excluded since is contained by
    # (baaab, 2, 6)
    assert len(pal_list) == 5
    assert pal_list[0] == ('aa', 0, 1)      # even palindrome btw 1,2
    assert pal_list[1] == ('aabaa', 0, 4)   # odd palindrome around 2
    assert pal_list[2] == ('aa', 3, 4)      # even palindrome btw 3,4
    assert pal_list[3] == ('baaab', 2, 6)   # even palindrome btw 3,4
    assert pal_list[4] == ('aa', 4, 5)      # even palindrome btw 4,5


def test_generate_palindromes_inner():
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('aba', False)
    ]
    assert len(pal_list) == 1
    assert pal_list[0] == ('aba', 0, 2)

    # Note: remember that all palindromes built around the same
    # central position must be returned
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('aabaa', False)
    ]
    assert len(pal_list) == 4
    assert pal_list[0] == ('aa', 0, 1)      # even palindrome btw 1,2
    assert pal_list[1] == ('aba', 1, 3)     # odd palindrome around 2
    assert pal_list[2] == ('aabaa', 0, 4)   # odd palindrome around 2
    assert pal_list[3] == ('aa', 3, 4)      # even palindrome btw 3,4
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('aabaaa', False)
    ]
    assert len(pal_list) == 6
    assert pal_list[0] == ('aa', 0, 1)      # even palindrome btw 1,2
    assert pal_list[1] == ('aba', 1, 3)     # odd palindrome around 2
    assert pal_list[2] == ('aabaa', 0, 4)   # odd palindrome around 2
    assert pal_list[3] == ('aa', 3, 4)      # even palindrome btw 3,4
    assert pal_list[4] == ('aaa', 3, 5)     # odd palindrome around 4
    assert pal_list[5] == ('aa', 4, 5)      # even palindrome btw 4,5
    pal_list = [
        p for p in palindromes.utils.generate_palindromes('aabaaab', False)
    ]
    assert len(pal_list) == 7
    assert pal_list[0] == ('aa', 0, 1)      # even palindrome btw 1,2
    assert pal_list[1] == ('aba', 1, 3)     # odd palindrome around 2
    assert pal_list[2] == ('aabaa', 0, 4)   # odd palindrome around 2
    assert pal_list[3] == ('aa', 3, 4)      # even palindrome btw 3,4
    assert pal_list[4] == ('aaa', 3, 5)     # odd palindrome around 4
    assert pal_list[5] == ('baaab', 2, 6)   # even palindrome btw 3,4
    assert pal_list[6] == ('aa', 4, 5)      # even palindrome btw 4,5
