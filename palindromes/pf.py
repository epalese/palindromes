from collections import defaultdict

import palindromes.utils


class Palindrome:
    """
    Utility class that keeps track of palindromes substrings.
    :param original_string: original string that contains the
    current palindrome.
    :type original_string: str
    :param start_position: start position in the original string where
    the palindrome is located.
    :type start_position: int
    :param end_position: end position in the original string where
    the palindrome is located.
    :type start_position: int
    """
    def __init__(self, original_string, start_position, end_position):
        if (
            original_string is None or
            start_position is None or
            end_position is None
        ):
            raise ValueError('Invalid argument(s)')
        self.original_string = original_string
        self.start_position = start_position
        self.end_position = end_position
        self.palindrome = self.original_string[
            self.start_position:self.end_position+1
        ]

    def __len__(self):
        return len(self.palindrome)

    def __str__(self):
        return "Text: {}, Index: {}, Length: {}".format(
            self.palindrome,
            self.start_position,
            len(self.palindrome)
        )

    def __repr__(self):
        return "<{}, sp={}, ep={}>".format(
            self.palindrome,
            self.start_position,
            self.end_position
        )

    def is_contained(self, other_palindrome):
        """
        Check if current palindrome is a substring of `other_palindrome`.
        """
        if (
            (self.start_position >= other_palindrome.start_position) and
            (self.end_position <= other_palindrome.end_position)
        ):
            return True
        return False


class LongestPalindromeFinder:
    """
    Class that find palindrome substrings contained
    in a string and return them in descending order of length
    (longest first).
    It has also the ability of returning only the palindrome
    substrings that are unique.
    `Unique` means that the palindrome is not substring of
    another palindrome.
    For example, given:
        - input string: 'aabaaaa'
        - palindromes are:
        1) ('aabaa', start_pos=0, end_pos=4)
        2) ('aaaa', start_pos=3, end_pos=6)
        The following are *all not unique* palindromes (since are substrings
        of the first palindrome 'aabaa'):
        - ('aa', start_pos=0, end_pos=1))
        - ('aba', start_pos=1, end_pos=3)
        - ('aa', start_pos=3, end_pos=4))
        The following are *all not unique* palindromes (since are substrings
        of second palindrome 'aaaa'):
        - ('aa', start_pos=3, end_pos=4))
        - ('aaa', start_pos=3, end_pos=5))
        - ('aa', start_pos=4, end_pos=5))
        - ('aaa', start_pos=4, end_pos=6))
        - ('aa', start_pos=5, end_pos=6))

        Note:
        The substring ('aaaa', start_pos=3, end_pos=6) is considered
        palindrome since it is not a substring of
        ('aabaa', start_pos=0, end_pos=4).
    """
    def __init__(self, string, exclude_inner=True):
        """
        Create an instance of LongestPalindromesFinder that keeps
        track of the `num_longest` longest unique palindromes.
        """
        self.string = string
        self.palindrome_by_length = defaultdict(lambda: [])
        self.lengths = []
        self._load_palindromes(exclude_inner)

    def _load_palindromes(self, exclude_inner):
        for palindrome_tuple in palindromes.utils.generate_palindromes(
            self.string, exclude_inner=exclude_inner
        ):
            _, start_pos, end_pos = palindrome_tuple
            palindrome = Palindrome(self.string, start_pos, end_pos)
            palindrome_length = len(palindrome)
            self.palindrome_by_length[palindrome_length] += [palindrome]
        self.lengths = list(self.palindrome_by_length.keys())
        self.lengths.sort(reverse=True)

    def _check_uniqueness(self, palindrome, list_of_palindromes):
        """
        Check if `palindrome` is unique with respect to palindromes
        in `list_of_palindromes`.
        Unique means that palindrome is not a string contained in
        another palindrome.
        """
        for lp in list_of_palindromes:
            if palindrome.is_contained(lp):
                return False
        return True

    def get_longest_palindromes(self, n=None, unique=True):
        """
        Return the first n longest palindrome strings.
        :param n: number of palindromes to return in descending
        order of length (longest first). If n is None or 0 all
        palindrome strings are returned.
        :type n: int
        :param unique: if True only unique palindrome
        """
        longest_palindromes = []
        for length in self.lengths:
            palindromes = self.palindrome_by_length[length]
            # get palindromes by length
            # and for each check if it is not contained
            # in longer palidromes that have been already
            # selected (if so, this means that current
            # palindrome is not unique)
            for p in palindromes:
                if unique:
                    if self._check_uniqueness(p, longest_palindromes):
                        longest_palindromes.append(p)
                else:
                    longest_palindromes.append(p)
                if n and len(longest_palindromes) == n:
                    return longest_palindromes
        return longest_palindromes
