def generate_palindromes(string, exclude_inner=True):
    """
    Return a list of tuples with all the palindrome strings in `string`.
    Each item in the list is a tuple (palindrome, start_pos, end_pos) where:
    palindrome: palindrome string
    start_pos: position where the palindrome starts in `string`
    end_pos: position where the palindrome ends in `string`
    The following holds always true:
    string[start_pos:end_pos+1] == palindrome
    Complexity: O(n^2)

    If exclude_inner is True only the longest palindrome is returned in a
    symmetric palindromic succession (i.e. all palindromes built around
    a given center position).
    Example:
        palindromic string: aabcbaa
        palindromes:
            - 'bcb' (len: 3) -> not returned since contained in palindromes
            'aabcbaa' and 'abcba' built around the same 'c' at position 3
            - 'abcba' (len: 5) -> not returned since contained in 'aabcbaa'
            built around the same 'c' at position 3
            - 'aabcbaa' (len: 7) ->  returned
            - 'aa' (starting at position 0) -> returned because is not part of
            the palindromic succession built around 'c' at position 3
            - 'aa' (starting at position 5) -> returned because is not part of
            the palindromic succession built around 'c' at position 3

    :param string: string to be searched for palindromes
    :type string: str
    :param exclude_inner: if inner palindromes have to be included
    :type exclude_inner: boolean
    :return: list of (palindrome, start_pos, end_pos)
    :rtype: list of tuples
    """
    if string is None:
        raise ValueError('A valid string must be provided.')
    for i in range(len(string)):
        if exclude_inner:
            yield from _generate_palindromes_no_inner(string, i)
        else:
            yield from _generate_palindromes(string, i)


def _generate_palindromes(string, i):
    """
    Generate odd and even palindromes centered around
    position i in the string.
    """
    yield from _find_palindromes(
        string=string,
        left_pos=i-1,
        right_pos=i+1
    )
    yield from _find_palindromes(
        string=string,
        left_pos=i,
        right_pos=i+1
    )


def _generate_palindromes_no_inner(string, i):
    """
    Same behaviour as :func:`_generate_palindromes` with the only difference
    that inner palindromes centered around position i are not returned.
    """
    palindrome = None
    # palindrome with odd number of characters
    # consume all palindromes discarding shorter ones
    # and return only the last (the longest)
    for p in _find_palindromes(
        string=string,
        left_pos=i-1,
        right_pos=i+1
    ):
        palindrome = p
    if palindrome:
        yield palindrome

    # palindrome with even number of characters
    # consume all palindromes discarding shorter ones
    # and return only the last (the longest)
    palindrome = None
    for p in _find_palindromes(
        string=string,
        left_pos=i,
        right_pos=i+1
    ):
        palindrome = p
    if palindrome:
        yield palindrome


def _find_palindromes(string, left_pos, right_pos):
    """
    Search for palindromic strings starting from `left_pos`
    and `right_pos` in the string and going outwards.
    The check implemented is string[left_pos] == string[right_pos]
    and the check expand around the initial values of `left_pos`
    and `right_pos` until the previous logical expression holds
    False.
    For example:
    - string: 'ccaabaacd
    - left_pos = 2
    - right_pos = 6
    Will produce:
    - ('aabaa', 2, 6)
    - ('caabaac', 1, 7)
    """
    while left_pos >= 0 and right_pos < len(string):
        if string[left_pos] != string[right_pos]:
            return
        yield (string[left_pos:right_pos+1], left_pos, right_pos)
        left_pos -= 1
        right_pos += 1


# def generate_palindromes_longest_first(string, min_legth=2):
#     """
#     Alternative method to generate palindrome strings starting from the
#     longest first.
#     The drawback of this method is that runtime complexity is O(n^4) while
#     :func:`find_palindromes` is O(n^2).
#     """
#     window = len(string)
#     while window >= min_legth:
#         i = 0
#         j = window - 1
#         while j < len(string):
#             is_pal = _check_palindrome(string[i:j+1])
#             if is_pal:
#                 yield string[i:j+1], i, j, len(string[i:j+1])
#             i += 1
#             j += 1
#         window -= 1


# def _check_palindrome(string):
#     i = 0
#     j = len(string) - 1
#     while i < j:
#         if string[i] != string[j]:
#             return False
#         i += 1
#         j -= 1
#     return True
