# Find Longest Unique Palindromes
This library allows to find the nth longest unique palindromes in a supplied string.

## Design choices
The code to find the 3 longest unique palindromes in a supplied string is shipped via a Python library.  

### Meaning of *unique palindrome*:  
`Unique` means that the palindrome is not a substring of another palindrome.
For example, given the following input string: `'aabaaaa'`, the unique palindromes are:  
1. 'aabaa', (start at position 0 and end at position 4)  
2. 'aaaa', (start at position 3 and end at position 6)  

The following are *all not unique* palindromes (since they are substrings of the first palindrome 'aabaa'):  
	* 'aa', (start at position 0, end at poition 1)  
	* 'aba', (start at position 1, end at position 3)  
	* 'aa', (start at position 3, end at position 4)  

The following are *all not unique* palindromes (since are substrings of the second palindrome 'aaaa'):  
	* 'aa', (start at position 3, end at position 4)  
	* 'aaa' (start at position 3, end at position 5)  
	* 'aa', (start at position 4, end at position 5)  
	* 'aaa', (start at position 4, end at position 6)  
	* 'aa', (start at position 5, end at position 6)  

Note:
The substring 'aaaa', that starts at position 3 and ends at position 6, is considered a palindrome since it is not
a substring of ('aabaa', start_pos=0, end_pos=4).

## Install
Assuming that [virtualenv](https://virtualenv.pypa.io/en/latest/) and
[virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)
are installed, create a virtual environment with Python 3:
```bash
$ mkvirtualenv -p python3 palindromes
```

Install *palindromes* from the git repository:
```bash
$ pip install git+https://epalese@bitbucket.org/epalese/palindromes.git
```

## Usage
After installing the library, start a Python3 session and run the following statements:
```python
>>> import palindromes.pf
>>> s = 'sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop'
>>> lpf = palindromes.pf.LongestPalindromeFinder(s)
>>> longest_palindromes = lpf1.get_longest_palindromes(n=3)
>>> for lp in longest_palindromes:
...     print(lp)
... 
Text: hijkllkjih, Index: 23, Length: 10
Text: defggfed, Index: 13, Length: 8
Text: abccba, Index: 5, Length: 6
```

## Tests
### Prerequisites
You need [docker](https://docs.docker.com/) and [docker-compose](https://docs.docker.com/compose/install/) to be 
installed on your local machine.

### Clone repository
Run:
``` bash
$ git clone https://epalese@bitbucket.org/epalese/palindromes.git
```

### Run tests
Run:
```bash
$ cd palindromes
palindromes$ docker-compose up --build
Building palindromes
Step 1/9 : FROM python:3.6-slim-stretch
 ---> ea57895cf3f9
Step 2/9 : ENV PYTHONPATH /src
 ---> Using cache
 ---> 0c0ff4a87077
Step 3/9 : ARG DEBIAN_FRONTEND=noninteractive
 ---> Using cache
 ---> 3e3fe6bd6559
Step 4/9 : RUN mkdir -p /src/palindromes
 ---> Using cache
 ---> 9862984f9f5a
Step 5/9 : WORKDIR /src
 ---> Using cache
 ---> 2c0d8736af6b
Step 6/9 : COPY requirements.txt .
 ---> Using cache
 ---> 3d4b3811f129
Step 7/9 : RUN pip install --upgrade pip && pip install     --no-cache-dir -r requirements.txt
 ---> Using cache
 ---> ad1e5e3a7689
Step 8/9 : COPY palindromes ./palindromes/
 ---> Using cache
 ---> c2672ff0c0f8
Step 9/9 : COPY tests ./tests
 ---> Using cache
 ---> 8b94db24dda8
Successfully built 8b94db24dda8
Successfully tagged palindromes_palindromes:latest
Starting palindromes_palindromes_1 ... done
Attaching to palindromes_palindromes_1
palindromes_1  | ============================= test session starts ==============================
palindromes_1  | platform linux -- Python 3.6.7, pytest-4.0.1, py-1.7.0, pluggy-0.8.0 -- /usr/local/bin/python
palindromes_1  | cachedir: .pytest_cache
palindromes_1  | rootdir: /src, inifile:
palindromes_1  | plugins: cov-2.6.0
palindromes_1  | collecting ... collected 17 items
palindromes_1  | 
palindromes_1  | tests/unit/test_pf.py::test_palindrome PASSED                            [  5%]
palindromes_1  | tests/unit/test_pf.py::test_LongestPalindromeFinder PASSED               [ 11%]
palindromes_1  | tests/unit/test_pf.py::test_example PASSED                               [ 17%]
palindromes_1  | tests/unit/test_utils.py::test__find_palindromes_empty PASSED            [ 23%]
palindromes_1  | tests/unit/test_utils.py::test__find_palindromes_boundaries PASSED       [ 29%]
palindromes_1  | tests/unit/test_utils.py::test__find_palindromes_len_1 PASSED            [ 35%]
palindromes_1  | tests/unit/test_utils.py::test__find_palindromes_even_palindromes PASSED [ 41%]
palindromes_1  | tests/unit/test_utils.py::test__generate_palindromes_generators PASSED   [ 47%]
palindromes_1  | tests/unit/test_utils.py::test__generate_palindromes_boundaries PASSED   [ 52%]
palindromes_1  | tests/unit/test_utils.py::test__generate_palindromes_start_index PASSED  [ 58%]
palindromes_1  | tests/unit/test_utils.py::test__generate_palindromes_no_inner_boundaries PASSED [ 64%]
palindromes_1  | tests/unit/test_utils.py::test__generate_palindromes_no_inner_start_index PASSED [ 70%]
palindromes_1  | tests/unit/test_utils.py::test_generate_palindromes_no_inner_call PASSED [ 76%]
palindromes_1  | tests/unit/test_utils.py::test_generate_palindromes_with_inner_call PASSED [ 82%]
palindromes_1  | tests/unit/test_utils.py::test_generate_palindromes_boundaries PASSED    [ 88%]
palindromes_1  | tests/unit/test_utils.py::test_generate_palindromes_no_inner PASSED      [ 94%]
palindromes_1  | tests/unit/test_utils.py::test_generate_palindromes_inner PASSED         [100%]
palindromes_1  | 
palindromes_1  | ----------- coverage: platform linux, python 3.6.7-final-0 -----------
palindromes_1  | Name                      Stmts   Miss  Cover
palindromes_1  | ---------------------------------------------
palindromes_1  | palindromes/__init__.py       0      0   100%
palindromes_1  | palindromes/pf.py            51      0   100%
palindromes_1  | palindromes/utils.py         28      0   100%
palindromes_1  | ---------------------------------------------
palindromes_1  | TOTAL                        79      0   100%
palindromes_1  | 
palindromes_1  | 
palindromes_1  | ========================== 17 passed in 0.10 seconds ===========================
palindromes_palindromes_1 exited with code 0
```

If the tests are successful, docker-compose should exit with status code 0.
